import { Map, GoogleApiWrapper } from 'google-maps-react';

const mapStyles = {
    width: '100%',
    height: '100%'
};

const MapsFromGoogle = () => {
    return (
        <Map
            google={google}
            zoom={14}
            style={mapStyles}
            initialCenter={{
                lat: 40.7128,
                lng: -74.0059
            }}
        />
    );
};

export default GoogleApiWrapper({
    apiKey: 'ICI_JE_METS_MA_CLE_API_PRIVATE'
  })(MapsFromGoogle);