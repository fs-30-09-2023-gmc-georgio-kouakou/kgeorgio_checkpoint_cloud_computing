import logo from './logo.svg';
import './App.css';
import MapsFromGoogle from './Components/MapsFromGoogle'; 

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <MapsFromGoogle />
      </header>
    </div>
  );
}

export default App;
